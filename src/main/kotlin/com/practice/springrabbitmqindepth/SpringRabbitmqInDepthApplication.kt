package com.practice.springrabbitmqindepth

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringRabbitmqInDepthApplication

fun main(args: Array<String>) {
	runApplication<SpringRabbitmqInDepthApplication>(*args)
}
