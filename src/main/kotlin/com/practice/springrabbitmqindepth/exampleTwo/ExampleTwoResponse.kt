package com.practice.springrabbitmqindepth.exampleTwo

data class ExampleTwoResponse(
        val example2EnqueueStatus: String
)