package com.practice.springrabbitmqindepth.exampleTwo

import com.practice.springrabbitmqindepth.utils.Constants.FAILURE
import com.practice.springrabbitmqindepth.utils.Constants.SUCCESS
import org.slf4j.LoggerFactory
import org.springframework.amqp.core.Message
import org.springframework.amqp.core.MessageDeliveryMode
import org.springframework.amqp.core.MessageProperties
import org.springframework.amqp.core.MessagePropertiesBuilder
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.stereotype.Service
import java.time.ZonedDateTime
import java.util.*

@Service
class ExampleTwoService(
        private val rabbitTemplate: RabbitTemplate
) {

    fun publishMessages():ExampleTwoServiceResponse {
        val configClassPublishResult = publishExample2()

        return ExampleTwoServiceResponse(configClassPublishResult)
    }

    /**
     * Note: Delivery Mode Persistence vs Queue Durability
     * Queue Durability: Whether or not the queue's definition should survive
     * a restart of the server or cluster.
     * Delivery Mode/Message Persistence: Whether a message should be persisted
     * on disk until it is consumed. - Lowers performance
     */

    /**
     * The fields set by the MessagePropertiesBuilder below are the
     * properties within the Header Frame Basic.Properties
     */

    /**
     * This doesn't get consumed anywhere. Doesn't do anything different on
     * the consumer end than Example 1 and it allows the expiration to demo.
     */

    private fun publishExample2(): String =
            try{
                // Application Use = Rabbit doesn't use it in any determinations. No formal definition.
                val messageProperties = MessagePropertiesBuilder.newInstance()
                    .setContentType(MessageProperties.CONTENT_TYPE_TEXT_PLAIN)
                    // For Application Use
                    .setMessageId("my-message-id")
                    // For Application Use
                    // Usually used for the originating message id in a response
                    // To indicate it is a response,
                    // Or as a transaction id for tracking across a workflow
                    .setCorrelationId("my-transaction-id")
                    // For Application Use
                    // Stored as an integer in Basic.Properties
                    .setTimestamp(Date(ZonedDateTime.now().toEpochSecond()))
                    // Stored as a string. Max Character length of 255
                    // Set in milliseconds
                    // If a message is published past the expiration time
                    // The message is not routed any where.
                    // Can also be set when declared or in the rabbit config file (policy)
                    // using the argument "x-message-ttl"
                    // Check in the RabbitMQ manager UI and the message will
                    // only last 10 seconds
                    .setExpiration("10000")
                    // Tells Rabbit to persist the message to disk or not
                    // If persisted the message will remain on disk until it gets consumed
                    // Two possible values: 1 or 2
                    // 1 = non-persisted
                    // 2 = persisted
                    .setDeliveryMode(MessageDeliveryMode.NON_PERSISTENT)
                    // Possible values: 0 - 9
                    // If a message with a priority of 9 gets enqueued,
                    // and then one with a 0. A newly connected consumer will
                    // pick up the 0 priority message first
                    .setPriority(2)
                    // End up in property 'headers'
                    // Free form property
                    // For app definitions, extra info,
                    // can be used for routing instead of routing keys
                    .setHeader("my-special-header-value1", 1)
                    .setHeader("my-special-header-value2", "hello")
                    .build()
                rabbitTemplate.send(
                        "exchange.example2.chapter3-example",
                        "example2.config-file.routing-key",
                        Message("Example 2 Message".toByteArray(), messageProperties)
                )
                SUCCESS
            } catch (e: Exception) {
                logger.error("Failed to publishExample2", e)
                "$FAILURE : ${e.message}"
            }

    private companion object {
        val logger = LoggerFactory.getLogger(ExampleTwoService::class.java)
    }

    data class ExampleTwoServiceResponse(
            val example2EnqueueStatus: String
    )
}