package com.practice.springrabbitmqindepth.exampleTwo

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class ExampleTwoController(
       private val exampleOneService: ExampleTwoService
) {

    @GetMapping("exampleTwo")
    fun getExampleTwo(): ResponseEntity<ExampleTwoResponse> {
        val publishResponse = exampleOneService.publishMessages()
        val responseBody = ExampleTwoResponse(
                publishResponse.example2EnqueueStatus
        )
        return ResponseEntity.ok(responseBody)
    }
}