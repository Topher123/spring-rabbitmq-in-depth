package com.practice.springrabbitmqindepth.utils

object Constants {
    const val FAILURE = "FAILURE"
    const val SUCCESS = "SUCCESS"
}