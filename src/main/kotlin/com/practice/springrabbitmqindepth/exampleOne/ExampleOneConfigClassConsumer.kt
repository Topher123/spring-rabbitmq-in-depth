package com.practice.springrabbitmqindepth.exampleOne

import org.slf4j.LoggerFactory
import org.springframework.amqp.core.Message
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.stereotype.Component
import com.practice.springrabbitmqindepth.exampleOne.ExampleOneConstants.EXAMPLE1_CONFIG_CLASS_QUEUE

@Component
class ExampleOneConfigClassConsumer {

    @RabbitListener(
            queues = [EXAMPLE1_CONFIG_CLASS_QUEUE]
    )
    fun consumeMessage(message: Message) {
        logger.info("ExampleOneConfigClassConsumer message received")

        try {
            val messageContents = String(message.body)
            logger.info("ExampleOneConfigClassConsumer message contents : $messageContents")
        } catch (e: Exception) {
            logger.error("ExampleOneConfigClassConsumer issue getting message body to string", e)
        }
    }

    private companion object {
        val logger = LoggerFactory.getLogger(ExampleOneConfigClassConsumer::class.java)
    }
}