package com.practice.springrabbitmqindepth.exampleOne

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class ExampleOneController(
       private val exampleOneService: ExampleOneService
) {

    @GetMapping("exampleOne")
    fun getExampleOne(): ResponseEntity<ExampleOneResponse> {
        val publishResponse = exampleOneService.publishMessages()
        val responseBody = ExampleOneResponse(
                publishResponse.queueConfigClassMessage,
                publishResponse.queueConfigFileMessage
        )
        return ResponseEntity.ok(responseBody)
    }
}