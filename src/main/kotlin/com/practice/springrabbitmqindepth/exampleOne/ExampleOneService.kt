package com.practice.springrabbitmqindepth.exampleOne

import com.practice.springrabbitmqindepth.exampleOne.ExampleOneConstants.EXAMPLE1_CONFIG_CLASS_EXCHANGE
import com.practice.springrabbitmqindepth.exampleOne.ExampleOneConstants.EXAMPLE1_CONFIG_CLASS_ROUTING_KEY
import com.practice.springrabbitmqindepth.exampleOne.ExampleOneConstants.EXAMPLE1_CONFIG_FILE_EXCHANGE
import com.practice.springrabbitmqindepth.exampleOne.ExampleOneConstants.EXAMPLE1_CONFIG_FILE_ROUTING_KEY
import com.practice.springrabbitmqindepth.utils.Constants.FAILURE
import com.practice.springrabbitmqindepth.utils.Constants.SUCCESS
import org.slf4j.LoggerFactory
import org.springframework.amqp.core.Message
import org.springframework.amqp.core.MessageProperties
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.stereotype.Service
import java.util.*

@Service
class ExampleOneService(
        private val rabbitTemplate: RabbitTemplate
) {

    fun publishMessages():ExampleOneServiceResponse {
        val configClassPublishResult = publishToConfigClassQueue()
        val configFilePublishResult = publishToConfigFileQueue()

        return ExampleOneServiceResponse(configClassPublishResult, configFilePublishResult)
    }

    private fun publishToConfigClassQueue(): String =
            try{
                rabbitTemplate.send(
                        EXAMPLE1_CONFIG_CLASS_EXCHANGE,
                        EXAMPLE1_CONFIG_CLASS_ROUTING_KEY,
                        Message("hello 1".toByteArray(), MessageProperties())
                )
                /**
                 * Although there is a queue called bound to this exchange,
                 * this call will not make it to the consumer since
                 * the queue is not bound the the routing key produced by
                 * UUID.randomUUID().toString()
                 */
                rabbitTemplate.send(
                        EXAMPLE1_CONFIG_CLASS_EXCHANGE,
                        UUID.randomUUID().toString(),
                        Message("hello 1.2".toByteArray(), MessageProperties())
                )
                SUCCESS
            } catch (e: Exception) {
                logger.error("Failed to publishToConfigClassQueue", e)
                "$FAILURE : ${e.message}"
            }

    private fun publishToConfigFileQueue(): String =
            try{
                rabbitTemplate.send(
                        EXAMPLE1_CONFIG_FILE_EXCHANGE,
                        EXAMPLE1_CONFIG_FILE_ROUTING_KEY,
                        Message("hello 2".toByteArray(), MessageProperties())
                )
                /**
                 * Although there is a queue called bound to this exchange,
                 * this call will not make it to the consumer since
                 * the queue is not bound the the routing key produced by
                 * UUID.randomUUID().toString()
                 */
                rabbitTemplate.send(
                        EXAMPLE1_CONFIG_FILE_EXCHANGE,
                        UUID.randomUUID().toString(),
                        Message("hello 2.2".toByteArray(), MessageProperties())
                )
                SUCCESS
            } catch (e: Exception) {
                logger.error("Failed to publishToConfigFileQueue", e)
                "$FAILURE : ${e.message}"
            }

    private companion object {
        val logger = LoggerFactory.getLogger(ExampleOneService::class.java)
    }

    data class ExampleOneServiceResponse(
            val queueConfigClassMessage: String,
            val queueConfigFileMessage: String
    )
}