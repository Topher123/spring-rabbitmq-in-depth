package com.practice.springrabbitmqindepth.exampleOne

object ExampleOneConstants {
    const val EXAMPLE1_CONFIG_CLASS_EXCHANGE = "exchange.example1.config-class.chapter2-example"
    const val EXAMPLE1_CONFIG_FILE_EXCHANGE = "exchange.example1.config-file.chapter2-example"

    const val EXAMPLE1_CONFIG_CLASS_QUEUE = "queue.example1.config-class"
    const val EXAMPLE1_CONFIG_FILE_QUEUE = "queue.example1.config-file"

    const val EXAMPLE1_CONFIG_CLASS_ROUTING_KEY = "example1.config-class.routing-key"
    const val EXAMPLE1_CONFIG_FILE_ROUTING_KEY = "example1.config-file.routing-key"
}