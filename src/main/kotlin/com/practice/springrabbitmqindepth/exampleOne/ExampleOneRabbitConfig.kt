package com.practice.springrabbitmqindepth.exampleOne

import com.practice.springrabbitmqindepth.exampleOne.ExampleOneConstants.EXAMPLE1_CONFIG_CLASS_EXCHANGE
import com.practice.springrabbitmqindepth.exampleOne.ExampleOneConstants.EXAMPLE1_CONFIG_CLASS_QUEUE
import com.practice.springrabbitmqindepth.exampleOne.ExampleOneConstants.EXAMPLE1_CONFIG_CLASS_ROUTING_KEY
import org.springframework.amqp.core.BindingBuilder
import org.springframework.amqp.core.DirectExchange
import org.springframework.amqp.core.Queue
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class ExampleOneRabbitConfig {

    @Bean
    fun createExample1Exchange(): DirectExchange =
            DirectExchange(EXAMPLE1_CONFIG_CLASS_EXCHANGE)

    @Bean
    fun createExample1Queue() =
            // durable = false : only persist in memory
            Queue(EXAMPLE1_CONFIG_CLASS_QUEUE, false)

    @Bean
    fun createExample1Binding(exchange: DirectExchange, queue: Queue) =
            BindingBuilder.bind(queue).to(exchange).with(EXAMPLE1_CONFIG_CLASS_ROUTING_KEY)

}