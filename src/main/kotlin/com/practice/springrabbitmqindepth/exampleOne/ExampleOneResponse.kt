package com.practice.springrabbitmqindepth.exampleOne

data class ExampleOneResponse(
        val queueConfigClassMessage: String,
        val queueConfigFileMessage: String
)