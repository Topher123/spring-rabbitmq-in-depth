package com.practice.springrabbitmqindepth.exampleOne

import com.practice.springrabbitmqindepth.exampleOne.ExampleOneConstants.EXAMPLE1_CONFIG_FILE_QUEUE
import org.slf4j.LoggerFactory
import org.springframework.amqp.core.Message
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.stereotype.Component

@Component
class ExampleOneConfigFileConsumer {

    @RabbitListener(
            queues = [EXAMPLE1_CONFIG_FILE_QUEUE]
             /*
             the commented out code below failed because
             I have a queue bean in the config class
             and this evidently attempted to make another bean without a qualifier
              */
//            bindings = [
//        QueueBindingg(
//                value = Queue(EXAMPLE1_CONFIG_FILE_QUEUE),
//                exchange = Exchange(EXAMPLE1_CONFIG_FILE_EXCHANGE),
//                // no type needed to be specified since this is a direct exchange
//                // and the default type is ExchangeTypes.DIRECT
//                key = [EXAMPLE1_CONFIG_FILE_ROUTING_KEY]
//        )
//    ]
    )
    fun consumeMessage(message: Message) {
        logger.info("ExampleOneConfigFileConsumer message received")

        try {
            val messageContents = String(message.body)
            logger.info("ExampleOneConfigFileConsumer message contents : $messageContents")
        } catch (e: Exception) {
            logger.error("ExampleOneConfigFileConsumer issue getting message body to string", e)
        }
    }

    private companion object {
        val logger = LoggerFactory.getLogger(ExampleOneConfigFileConsumer::class.java)
    }
}