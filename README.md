# Spring RabbitMQ In Depth

###RabbitMQ In Depth book done with kotlin and springboot

RabbitMQ Login:
* User name: guest
* Password: guest

RabbitMQ Local management URL:
* http://localhost:15672

Start RabbitMQ container from project root:
* cd docker
* docker-compose up rabbitmq